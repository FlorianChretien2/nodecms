var base = require('../config/db.js');

var ArticleModel = {
    
    getArticles(callback) {
        return base('article').select().firstPage(callback);
    },

    getArticlesBySlug(slug,callback) {
        return base('article').select({
            maxRecords:1,
            filterByFormula: '{slug} = "'+ slug + '"'
        }).firstPage(callback);
    }
}

module.exports = ArticleModel;