var CategoryModel = {
    getCategory(id) {
        return null;
    },
    getCategory() {
        return [
            {
                "id": "recGGktIMunhkJXMf", "fields":
                {
                    "article": ["rec7zcZlR2nUnK8UP"],
                    "slug": "chien",
                    "type": "Chien",
                    "id": 1
                },
                "createdTime": "2017-11-16T10:21:23.000Z"
            },
            {
                "id": "recoNVZWn8Wen3p4r", "fields":
                {
                    "article": ["recab9axwwIss6wFQ", "recakBKrrVTw0Dl03"],
                    "slug": "chat",
                    "type": "Chat",
                    "id": 2
                },
                "createdTime": "2017-11-16T10:21:23.000Z"
            },
            {
                "id": "reczTp1vvhHn87x8C", "fields":
                {
                    "article": ["reccjIwjTGHLnJQFE"],
                    "slug": "poissson",
                    "type": "Poisson",
                    "id": 3
                },
                "createdTime": "2017-11-16T10:21:23.000Z"
            }
        ]
    }
};

module.exports = CategoryModel;