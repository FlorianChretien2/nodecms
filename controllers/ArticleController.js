var Article = require('../models/Article.js')

var ArticleController = {
    
    index(req, res, next) {
        Article.getArticles(function(err,records) {
            if (err) { 
                res.status(404).send('Erreur avec airtable'); 
            }
            else{
                res.articles = records;
                next();
            }
        });
    },
    show(req, res, next) {
        Article.getArticlesBySlug(req.params.slug, function (err,records) {
            if (err) { 
                res.status(404).send('Erreur avec airtable');
            }
            else {
                res.article = records[0];
                next();
            }
        });
    }
    
};

module.exports = ArticleController;
