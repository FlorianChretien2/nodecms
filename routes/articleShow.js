var express = require('express');
var router = express.Router();

var ArticleController = require('../controllers/ArticleController.js');

router.get('/articles/:slug', ArticleController.show, function (req, res, next) {
    var article = res.article;
    res.render('article/show',{ 
        title   : article.fields.title, 
        article : article 
    });
});

module.exports = router;