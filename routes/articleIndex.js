var express           = require('express');
var router            = express.Router();
var ArticleController = require('../controllers/ArticleController.js');

router.get('/', ArticleController.index, function (req, res, next) {
    var articles = res.articles;
    res.render('article/index',{ 
        title   : 'Liste des articles', 
        articles: articles 
    });
});

module.exports = router;