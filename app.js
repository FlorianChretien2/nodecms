var express      = require('express');
var http         = require('http');
var path         = require('path');
var logger       = require('morgan');
var bodyParser   = require('body-parser');
var cookieParser = require('cookie-parser');
var session      = require('express-session');
var errorhandler = require('errorhandler')
var config       = require('./config')();

var app = express();

app.set('views', __dirname + '/views');
app.set('view engine', 'pug');

var ArticleController = require('./controllers/ArticleController.js');

var routeArticleIndex = require('./routes/articleIndex.js');
var routeArticleShow  = require('./routes/articleShow.js');
// all environments
// app.set('port', process.env.PORT || 3000);

// app.use(express.favicon());
app.use(logger("combined"));
app.use(bodyParser.json());
// app.use(express.methodOverride());
app.use(cookieParser('fast-delivery-site'));
app.use(session({
	secret: 'cookie_secret',
	name: 'cookie_name',
	// store: sessionStore, // connect-mongo session store
	// proxy: true,
	resave: true,
	saveUninitialized: true
}));

// app.use(app.router);
app.use(require('less-middleware')({ src: __dirname + '/public' }));
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
	app.use(errorhandler());
}

// var middleware = function (req, res, next) {
// 	next(); 
// };

app.use(routeArticleIndex);
app.use(routeArticleShow);


// app.get('/', middleware, function (req, res, next) {
// 	ArticleController.index(req, res, next);
// });



http.createServer(app).listen(config.port, function () {
	console.log(
		'\nExpress server listening on port ' + config.port
	);
});
